import ProductModel from '../models/Product.js';

async function getProductModel() {
	const ProductModels = await ProductModel.find({});
	return ProductModels;
}

async function getSearchProductModel(keyword, limit) {
	const SearchProductModels = await ProductModel.find({
		brandName: { $regex: keyword, $options: 'i' },
	}).limit(limit);

	return SearchProductModels;
}

class NewController {
	product(req, res) {
		getProductModel().then(function (product) {
			res.set({
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*',
			});
			res.send(product);
		});
	}

	searchproduct(req, res) {
		try {
			const keyword = req.param('brandName');
			const limit = req.param('limit'); // Lấy giá trị của tham số tìm kiếm 'keyword'

			if (keyword === '') {
				return res
					.set({
						'Content-Type': 'application/json',
						'Access-Control-Allow-Origin': '*',
					})
					.status(200)
					.send([]); // Trả về một mảng rỗng nếu không có giá trị keyword
			} else {
				getSearchProductModel(keyword, limit).then(function (product) {
					res.set({
						'Content-Type': 'application/json',
						'Access-Control-Allow-Origin': '*',
					});
					res.send(product);
				});
			}
		} catch (error) {
			console.error('Lỗi khi tìm kiếm:', error);
			res.status(500).json({ error: 'Lỗi khi tìm kiếm' });
		}
	}
}

export default new NewController();
