import VoucherModel from '../models/Voucher';

async function getSearchVoucherModel(keyword) {
	const SearchVoucherModels = await VoucherModel.find({
		maVoucher: { $regex: keyword, $options: 'i' },
	});

	return SearchVoucherModels;
}

class VoucherController {
	voucher(req, res) {
		try {
			const keyword = req.param('maVoucher');

			if (keyword === '') {
				return res
					.set({
						'Content-Type': 'application/json',
						'Access-Control-Allow-Origin': '*',
					})
					.status(200)
					.send([]); // Trả về một mảng rỗng nếu không có giá trị keyword
			} else {
				getSearchVoucherModel(keyword).then(function (voucher) {
					res.set({
						'Content-Type': 'application/json',
						'Access-Control-Allow-Origin': '*',
					});
					res.send(voucher);
				});
			}
		} catch (error) {
			console.error('Lỗi khi tìm kiếm:', error);
			res.status(500).json({ error: 'Lỗi khi tìm kiếm' });
		}
	}
}

export default new VoucherController();
