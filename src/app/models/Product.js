import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const Product = new Schema({
	image: { type: String },
	alt: { type: String },
	brandName: { type: String },
	title: { type: String },
	price: { type: Object },
	path: { type: String },
	description: { type: String },
});

const ProductModel = mongoose.model('Product', Product, 'product');

export default ProductModel;
