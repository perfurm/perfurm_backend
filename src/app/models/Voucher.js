import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const Voucher = new Schema({
	maVoucher: { type: String },
	phanTramGiamGia: { type: String },
});

const VoucherModel = mongoose.model('Voucher', Voucher, 'voucher');

export default VoucherModel;
