import path from 'path';
import { fileURLToPath } from 'url';
import express from 'express';
import { engine } from 'express-handlebars';
import morgan from 'morgan';
import Route from './routes/index.js';
import Connect from './config/db/index.js';
const app = express();
const port = 3008;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

app.use(morgan('combined'));
app.engine(
	'hbs',
	engine({
		extname: '.hbs',
	})
);
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'resource/view'));

Connect();
Route(app);

app.listen(port, () => {
	console.log(`Example app listening on port ${port}`);
});
