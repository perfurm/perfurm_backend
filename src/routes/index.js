import productRouter from './product';
import voucherRouter from './voucher';

function Route(app) {
	app.get('/', (req, res) => {
		res.render('home');
	});

	app.use('/product', productRouter);

	app.use('/voucher', voucherRouter);
}
export default Route;
