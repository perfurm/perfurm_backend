import express from 'express';
import NewController from '../app/controllers/NewController.js';
const productRouter = express.Router();

productRouter.get('/productdb', NewController.product);
productRouter.get('/searchproductdb', NewController.searchproduct);

export default productRouter;
