import express from 'express';
import VoucherController from '../app/controllers/VoucherController';
const voucherRouter = express.Router();

voucherRouter.get('/', VoucherController.voucher);

export default voucherRouter;
